/*
 * Este código foi desenvolvido baseado no algoritmo ABC (artificial Bee Colony)
 * O código que serviu de base para este pode ser encontrado em:
 * http://mf.erciyes.edu.tr/abc/
 * Trechos foram transcritos de forma literal.
 *
 * Nenhum direito reservado.
 */
/**
 * @author Walisson Gomes
 * @see http://mf.erciyes.edu.tr/abc/ for original code where this one was based
 */

package ABC;
import Random.RandomPool;
import Main.Problem;

public class BeeColony {
    
    /* Default Control Parameters of ABC algorithm*/
    private int NP; /* The number of colony size (employed bees+onlooker bees)*/
    private int NE; /*The number of food sources (employed bees) equals the half of the colony size*/
    private int limit;  /*A food source which could not be improved through "limit" trials is abandoned by its employed bee*/
    private int maxInter; /*The number of cycles for foraging {a stopping criteria}*/
    private int iteration; /*Current iteration*/

    private Bee employed[];

    private Problem pb;    /*Problem to be optimized*/
    private double GlobalMin;      /*Optimum solution obtained by ABC algorithm*/
    private double GlobalMinPos[]; /*Parameters of the optimum solution*/

    public BeeColony(int NP, int limit, int maxInter, int pbIdx) {
        
        pb = new Problem();
        pb.setProblem(pbIdx);
        
        this.NP = NP;
        this.NE = this.NP/2;
        this.limit = limit;
        this.maxInter = maxInter;
        this.iteration = 0;
        
        GlobalMinPos = new double[pb.getD()];
        
        this.employed = new Bee[this.NE];
        for(int i = 0; i < this.NE; i++)
            this.employed[i] = new Bee();
        
        init();
        MemorizeBestSource();
    }
    
    public void iterate(){
        
        SendEmployedBees();
        SendOnlookerBees();
        MemorizeBestSource();
        SendScoutBees();
        
        iteration++;
    }
    
    public boolean hasFinished(){
        return iteration >= maxInter;
    }
    
    public void execute(){
        while(!hasFinished()){
            //System.out.println(GlobalMin);
            iterate();
        }
        //System.out.println("");
    }
    
    /*Variables are initialized in the range [lb,ub]. If each parameter has different range, use arrays lb[j], ub[j] instead of lb and ub */
    /* Counters of food sources are also initialized in this function*/
    /*All food sources are initialized */
    private void init(){

        for(int i = 0; i < NE; i++)
            employed[i].initLocation(pb.getD(), pb.getUB(), pb.getLB());
        
        for(int i = 0; i < NE; i++){
            double loc[] = employed[i].getLocation();
            double f = calculateFunction(loc);
            employed[i].setFuncValue(f);
            employed[i].setFitness(CalculateFitness(f));
        }
	
        GlobalMin = employed[0].getFuncValue();
	GlobalMinPos = employed[0].getLocation();
    }
    
    /*The best food source is memorized*/
    private void MemorizeBestSource(){
	
	for(int i = 0; i < NE; i++){
            if(employed[i].getFuncValue() < GlobalMin){
	        GlobalMin = employed[i].getFuncValue();
	        GlobalMinPos = employed[i].getLocation();
	    }
        }
    }
    
    public double[] getGlobalMinPos(){
        return GlobalMinPos;
    }
    
    public double getGlobalMin(){
        return GlobalMin;
    }

    private void SendEmployedBees(){
        
	/*Employed Bee Phase*/
	for(int i = 0 ; i < NE; i++){
	    double mutSol[] = mutateSolution(i);
	    
            double ObjValSol = calculateFunction(mutSol);
	    double FitnessSol = CalculateFitness(ObjValSol);
	        
	    /*a greedy selection is applied between the current solution i and its mutant*/
	    if (FitnessSol > employed[i].getFitness()){
	        /*If the mutant solution is better than the current solution i, replace the solution with the mutant and reset the trial counter of solution i*/
	        employed[i].resetTrial();
	        employed[i].setLocation(mutSol);
                employed[i].setFuncValue(ObjValSol);
                employed[i].setFitness(FitnessSol);
	    }else{   /*if the solution i can not be improved, increase its trial counter*/
	        employed[i].increaseTrial();
	    }
        }
    }

    private void SendOnlookerBees(){

        /* A food source is chosen with the probability which is proportioal to its quality*/
        /*Different schemes can be used to calculate the probability values*/
        /*For example prob(i)=fitness(i)/sum(fitness)*/
        /*or in a way used in the metot below prob(i)=a*fitness(i)/max(fitness)+b*/
        /*probability values are calculated by using fitness values and normalized by dividing maximum fitness value*/
        double prob[] = new double[this.NE];
	double maxfit = 0.0;
        for (int i = 1; i < NE; i++)
	    maxfit = employed[i].getFitness();
	        
	for (int i = 0; i < NE; i++)
            prob[i]=(0.9*(employed[i].getFitness()/maxfit))+0.1;
        
        int i = 0;
        int t = 0;
	/*onlooker Bee Phase*/
	while(t < NE){

            double r = RandomPool.r().nextDouble();
	    if(r < prob[i]){ /*choose a food source depending on its probability to be chosen*/
	        t++;
	        
	        double mutSol[] = mutateSolution(i);
	    
                double ObjValSol = calculateFunction(mutSol);
	        double FitnessSol = CalculateFitness(ObjValSol);
                
	        /*a greedy selection is applied between the current solution i and its mutant*/
	        if (FitnessSol > employed[i].getFitness()){
                    /*If the mutant solution is better than the current solution i, replace the solution with the mutant and reset the trial counter of solution i*/
                    employed[i].resetTrial();
                    employed[i].setLocation(mutSol);
                    employed[i].setFuncValue(ObjValSol);
                    employed[i].setFitness(FitnessSol);
	        }else{   /*if the solution i can not be improved, increase its trial counter*/
	            employed[i].increaseTrial();
	        }
	    }
	    
            i++;
	    if(i == NE)
                i = 0;
        }
    }

    /*determine the food sources whose trial counter exceeds the "limit" value. In Basic ABC, only one scout is allowed to occur in each cycle*/
    private void SendScoutBees(){
        
	int maxtrialindex = 0;
        
	for (int i = 1; i < NE; i++){
	    if (employed[i].getTrial() > employed[maxtrialindex].getTrial())
                maxtrialindex = i;
	}
	if(employed[maxtrialindex].getTrial() >= limit)
            employed[maxtrialindex].initLocation(pb.getD(), pb.getUB(), pb.getLB());
    }
    
    private double[] mutateSolution(int i){
        
        double solution[] = new double[pb.getD()];
        
        /*The parameter to be changed is determined randomly*/
        double r = RandomPool.r().nextDouble();
        int param2change = (int)(r*pb.getD());
        /*param2change corrresponds to j, neighbour corresponds to k in equation v_{ij}=x_{ij}+\phi_{ij}*(x_{kj}-x_{ij})*/

        /*A randomly chosen solution is used in producing a mutant solution of the solution i*/
        r = RandomPool.r().nextDouble();
        int neighbour = (int)(r*NE);
        /*Randomly selected solution must be different from the solution i*/        
        while(neighbour == i)
            neighbour=(int)(RandomPool.r().nextDouble()*NE);
        
        double loc2[] = employed[neighbour].getLocation();
        double loc[]  = employed[i].getLocation();
        for(int j = 0; j < pb.getD(); j++)
            solution[j] = loc[j];

        /*v_{ij}=x_{ij}+\phi_{ij}*(x_{kj}-x_{ij}) */
        r = RandomPool.r().nextDouble();
        solution[param2change] = loc[param2change] + (loc[param2change] - loc2[param2change]) * (r-0.5)*2;

        /*if generated parameter value is out of boundaries, it is shifted onto the boundaries*/
        if (solution[param2change] < pb.getLB())
            solution[param2change] = pb.getLB();
        if (solution[param2change] > pb.getUB())
            solution[param2change] = pb.getUB();

        return solution;
    }
    
    /*Fitness function*/
    private double CalculateFitness(double fun){
        double result=0;
        if(fun >= 0)
            result= 1 / (fun+1);
        else
            result = 1 + Math.abs(fun);
	
        return result;
    }

    private double calculateFunction(double sol[]){
        return pb.toOptimize(sol);
    }
}
