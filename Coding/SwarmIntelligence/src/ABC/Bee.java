
package ABC;

import Random.RandomPool;

public class Bee {
    
    double location[];
    double funcValue;
    double fitness;
    int D;
    int trial;
    
    Bee(){
        funcValue = 0.0;
        fitness = 0.0;
        trial = 0;
        D = 0;
    }
    
    void initLocation(int size, double ub, double lb){
        
        this.D = size;
        this.location = new double[size];
        
        for (int j = 0; j < D; j++){
            double r = RandomPool.r().nextDouble();
	    this.location[j] = r * (ub-lb) + lb;
	}
    }
    
    void initLocation(int size, double ub[], double lb[]){
        
        if(size != ub.length || size != lb.length){
            System.err.println("initialization Problem!");
        }
        
        this.D = size;
        this.location = new double[size];
        
        for (int j = 0; j < D; j++){
            double r = RandomPool.r().nextDouble();
	    this.location[j] = r * (ub[j]-lb[j]) + lb[j];
	}
    }
    
    void setLocation(double newLoc[]){
        
        this.location = new double[D];
        for(int i = 0; i < D; i++)
            this.location[i] = newLoc[i];
    }
    
    double[] getLocation(){
        double[] locationAux = new double[D];
        for(int i = 0; i < D; i++)
            locationAux[i] = location[i];
        return locationAux;
    }
    
    void setFuncValue(double f){
        this.funcValue = f;
    }
    
    double getFuncValue(){
        return this.funcValue;
    }
    
    void setFitness(double fitness){
        this.fitness = fitness;
    }
    
    double getFitness(){
        return this.fitness;
    }
    
    void increaseTrial(){
        this.trial++;
    }
    
    void resetTrial(){
        this.trial = 0;
    }
    
    int getTrial(){
        return this.trial;
    }
}
