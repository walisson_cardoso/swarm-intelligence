
package PSO;

import Main.Problem;
import Random.RandomPool;

public class PSO {
    
    /*Default Parameters for PSO algorithm*/
    private int NP;         //Number of Particles
    private int maxInter;   //Number of iterations to finish
    private int iteration; //Current iteration
    
    //Algorithm constants
    private double w  = 0.7;
    private double c1 = 1.4;
    private double c2 = 0.8;
    
    private Problem pb;            //Problem to be optimized
    private double globalFit;      //Min value of the function found
    private double globalMinPos[]; //Function arguments that generates the min value
    private Particle particles[];  //Swarm
    
    
    
    public PSO(int NP, int maxInter, int pbIdx){
        
        this.pb = new Problem();
        pb.setProblem(pbIdx); //Problem index
        
        this.NP = NP;
        this.maxInter = maxInter;
        this.iteration = 0;
        
        this.globalMinPos = new double[pb.getD()];
        
        this.particles = new Particle[NP];
        for(int i = 0; i < NP; i++)
            this.particles[i] = new Particle();
        
        init();
        updateFitness();
    }
    
    /*Main functions for execution*/
    
    public void iterate(){
        
        int bestIndex = 0;
        
        for(int i = 0; i < NP; i++){
            //Update best location
            if(particles[i].getBestFitness() < particles[i].getFitness()){
                double loc[] = particles[i].getLocation();
                double fit   = particles[i].getFitness();
                particles[i].setBestLocation(fit, loc);
            }
            
            //Best particle at this iteration
            if(particles[bestIndex].getFitness() < particles[i].getFitness()){
                bestIndex = i;
            }
        }
        
        for(int i = 0; i < NP; i++){
            
            double bLoc[] = particles[i].getBestLocation();
            double loc[]  = particles[i].getLocation();
            double best[] = globalMinPos;
            
            //Update velocity
            for(int j = 0; j < pb.getD(); j++){
                
                double r1 = RandomPool.r().nextDouble();
                double r2 = RandomPool.r().nextDouble();
                
                double vel = particles[i].getVelocity(j);
                vel = w*vel + (c1*r1)*(bLoc[j]-loc[j]) + (c2*r2)*(best[j]-loc[j]);
                
                particles[i].setVelocity(vel, j);
            }
            
            //Update location
            for(int j = 0; j < pb.getD(); j++){
                double newPos = particles[i].getLocation(j) + particles[i].getVelocity(j);
                particles[i].setLocation(newPos, j);
            }
        }
        
        updateFitness();
        
        iteration++;
    }
    
    public boolean hasFinished(){
        return iteration >= maxInter;
    }
    
    public void execute(){
        while(!hasFinished())
            iterate();
    }
    
    
    /*Variables are initialized in the range [lb,ub]. If each parameter has different range, use arrays lb[j], ub[j] instead of lb and ub */
    public void init(){
        
        for(int i = 0; i < NP; i++)
            particles[i].init(pb.getD(), pb.getUB(), pb.getLB());
                
        globalFit = Double.NEGATIVE_INFINITY;
    }
    
    public void setConstants(double w, double c1, double c2){
        this.w  = w;
        this.c1 = c1;
        this.c2 = c2;
    }
    
    private void updateFitness(){
	
	for(int i = 0; i < NP; i++){
            
            double loc[] = particles[i].getLocation();
            double v = calculateFunction(loc);
            double f = CalculateFitness(v);
            particles[i].setFuncVal(v);
            particles[i].setFitness(f);
            
            if(particles[i].getFitness() > globalFit){
	        globalFit = particles[i].getFitness();
	        globalMinPos = particles[i].getLocation();
	    }
        }
    }
    
    public double[] getGlobalMinPos(){
        return globalMinPos;
    }
    
    public double getGlobalFit(){
        return globalFit;
    }
    
    /*Fitness function*/
    private double CalculateFitness(double fun){
        //Fitness to be maximized
        return -fun;
    }

    private double calculateFunction(double sol[]){
        return pb.toOptimize(sol);
    }
    
    public void setProblem(int idx){
        pb.setProblem(idx);
    }
}
