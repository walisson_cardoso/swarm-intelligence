/*
 * This is the agent from the PSO algorithm
 * 
 */
package PSO;

import Random.RandomPool;

public class Particle {
    
    //Particle's data
    private double bestLocation[]; //Best particles position in history
    private double location[];     //Actual location
    private double velocity[];     //Actual velocity
    private double funcVal;        //Function value associated with this solution
    private double bestFit;        //Fitness in bestLocation time
    private double fitness;        //Fitness associated with the current solution
    private double ub[];           //Upperbound for each dimention
    private double lb[];           //Lower bound for each dimention
    private int size;              //Size (how many dimentions does a solution have?)
    
    public Particle(){
        //Nothing to be done here
    }
    
    //If the limits are equal in all dimentions, this function can be called
    //The generalized version is then used
    public void init(int size, double ub, double lb){
        
        double aub[] = new double[size], alb[] = new double[size];
        
        for(int i = 0; i < size; i++){
            aub[i] = ub;
            alb[i] = lb;
        }
        
        init(size, aub, alb);
    }
    
    public void init(int size, double ub[], double lb[]){
        
        this.bestLocation = new double[size];
        this.location  = new double[size];
        this.velocity  = new double[size];
        this.ub        = new double[size];
        this.lb        = new double[size];
        this.funcVal   = Double.NEGATIVE_INFINITY;
        this.bestFit   = Double.NEGATIVE_INFINITY;
        this.fitness   = Double.NEGATIVE_INFINITY;
        this.size      = size;
        
        if(lb.length != size || ub.length != size)
            System.err.println("Particle.init(): Invalid interval!");
        
        for(int i = 0; i < size; i++){
            if(lb[i] > ub[i])
                System.err.println("Particle.init(): Invalid interval!");
            this.ub[i] = ub[i];
            this.lb[i] = lb[i];
            
            double r1 = RandomPool.r().nextDouble();
            double r2 = RandomPool.r().nextDouble();
            
            this.location[i] = r1*(ub[i]-lb[i]) + lb[i];
            this.velocity[i] = r2*(ub[i]-lb[i]) + lb[i];
            this.bestLocation[i] = this.location[i];
        }
    }
    
    public double[] getLocation() {
        //Avoid hard copy
        double loc[] = new double[size];
        for(int i = 0; i < size; i++)
            loc[i] = location[i];
        
        return loc;
    }
    
    public double getLocation(int D) {
        return location[D];
    }

    public void setLocation(double[] location) {
        //Any location can be set here
        //Avoid hard copy
        for(int i = 0; i < size; i++)
            this.location[i] = location[i];
    }

    public void setLocation(double location, int D) {
        //Adjust of location in one dimention
        //This will not allow user to set a value outside the range
        if(location > ub[D])
            this.location[D] = ub[D];
        else if(location < lb[D])
            this.location[D] = lb[D];
        else
            this.location[D] = location;
    }
    
    public double getBestFitness(){
        return bestFit;
    }
    
    public double[] getBestLocation() {
        //Avoid hard copy
        double loc[] = new double[size];
        for(int i = 0; i < size; i++)
            loc[i] = bestLocation[i];
        
        return loc;
    }

    public void setBestLocation(double bestFit, double[] bestLocation) {
        this.bestFit = bestFit;
        this.bestLocation = bestLocation;
    }

    public double[] getVelocity() {
        //Avoid hard copy
        double loc[] = new double[size];
        for(int i = 0; i < size; i++)
            loc[i] = velocity[i];
        
        return loc;
        
    }

    public double getVelocity(int D){
        return velocity[D];
    }

    public void setVelocity(double[] velocity) {
        //Avoid hard copy
        for(int i = 0; i < size; i++)
            this.velocity[i] = velocity[i];
    }
    
    public void setVelocity(double velocity, int D) {
        this.velocity[D] = velocity;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public double getFuncVal() {
        return funcVal;
    }

    public void setFuncVal(double funcVal) {
        this.funcVal = funcVal;
    }
    
    public double getSize(){
        return size;
    }
}
