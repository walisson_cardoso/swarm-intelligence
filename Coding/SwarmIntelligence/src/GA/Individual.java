/*
 * Single individual on a GA
 */
package GA;

/**
 * @author Walisson C. Gomes
 */
public class Individual {
    
    private double fitness;
    private  int crmSize;
    private int cromossome[];

    public Individual(int crmSize) {
        this.crmSize = crmSize;
        cromossome = new int[crmSize];
    }
    
    public void initIndividual(){
        
        for(int i = 0; i < crmSize; i++)
            cromossome[i] = Random.RandomPool.r().nextInt(2);
    }
    
    public void initIndividual(int crmSize){
        this.crmSize = crmSize;
        this.cromossome = new int[crmSize];
        
        for(int i = 0; i < crmSize; i++)
            cromossome[i] = Random.RandomPool.r().nextInt(2);
    }
      
    public Individual copy(){
        Individual cp = new Individual(this.crmSize);
        cp.setFitness(this.fitness);
        
        for(int i = 0; i < crmSize; i++)
            cp.setGene(i, this.getGene(i));
        
        return cp;
    }
    
    public void setGene(int genePos, int value){
        if((genePos >= 0 && genePos < crmSize) && (value == 0 || value == 1))
            cromossome[genePos] = value;
        else
            System.out.println("setGene: invalid value or position");
    }
    
    public int getGene(int genePos){
        if(genePos >= 0 && genePos < crmSize)
            return cromossome[genePos];
        else
            System.out.println("getGene: invalid Position");
        return 0;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public double getFitness() {
        return fitness;
    }

    public void setCromossome(int[] cromossome) {
        if(crmSize != cromossome.length){
            this.crmSize = cromossome.length;
            this.cromossome = new int[this.crmSize];
        }
        
        for(int i = 0; i < crmSize; i++){
            this.cromossome[i] = cromossome[i]; // No hard copy
        }
    }
    
    public int[] getCromossome() {
        int temp[] = new int[crmSize]; //No hard copy
        for(int i = 0; i < crmSize; i++)
            temp[i] = cromossome[i];
        
        return temp;
    }
    
    public String getStrCromossome() {
        String crmStr = "";
        for(int i = 0; i < crmSize; i++)
            if(cromossome[i] == 0)
                crmStr += "0";
            else
                crmStr += "1";
        
        return crmStr;
    }
    
    public void print(){
        for(int i = 0; i < crmSize; i++)
            System.out.print(cromossome[i] + " ");
        System.out.println();
    }
}
