/*
 * Code that implements a simple genetic algorithm
 */
package GA;
import Random.RandomPool;
import Main.Problem;

/**
 * @author Walisson Cardoso Gomes
 */
public class GA {
    
    private int popSize;
    private int genSize;
    private int maxInter;  /*The number of cycles for foraging {a stopping criteria}*/
    private int iteration; /*Current iteration*/
    private Individual population[];
    
    private double crossoverProb = 0.9;
    private double mutationProb = 0.01;
    
    private double minFit;
    private double meanFit;
    private double maxFit;

    private Problem pb;    /*Problem to be optimized*/
    private double globalMin;      /*Optimum solution obtained by GA algorithm*/
    private double globalMinPos[]; /*Parameters of the optimum solution*/

    public GA(int popSize, int genSize, int maxInter, int pbIdx) {
        
        this.pb = new Problem();
        pb.setProblem(pbIdx);
        
        this.popSize = popSize;
        this.genSize = genSize;
        this.maxInter = maxInter;
        
        this.globalMin = Double.POSITIVE_INFINITY;
        this.globalMinPos = new double[pb.getD()];
        
        population = new Individual[popSize];
        for(int i = 0; i < popSize; i++){
            population[i] = new Individual(genSize);
            population[i].initIndividual();
        }
        
        updateFitness();
    }
    
    
    public void execute(){
        while(!hasFinished())
            iterate();
    }
    
    public void iterate(){
        
        //Selection
        //Roulette Operator
        double sumFitness = 0.0;
            //+minFit: Assegura que mesmo fitnesses negativos podem ser
            //usados no algoritmo. Não altera a probabilidade de escolha
            //Se minFit negativo
        double comp = Math.abs(minFit);
        for(int i = 0; i < popSize; i++)
            sumFitness += population[i].getFitness()+comp;
        
        
        Individual selectedInds[] = new Individual[popSize];
        for(int i = 0; i < popSize; i++) {
            double selected = sumFitness * RandomPool.r().nextDouble(true,true);
            double wheel = 0.0;
            int index = 0;
            for(int j = 0; j < popSize; j++){
                wheel += population[j].getFitness()+comp;
                if(wheel >= selected){
                    index = j;
                    break;
                }
            }
            selectedInds[i] = population[index].copy();
        }
        
        //Crossover
        //One Point Crossover
        for(int i = 0; i < popSize - 1; i += 2){
            double r = RandomPool.r().nextDouble(true,true);
            int cutPoint = RandomPool.r().nextInt(genSize-1);
            if(r < crossoverProb){
                for(int j = 0; j <= cutPoint; j++){
                    int gene1 = selectedInds[i].getGene(j);
                    int gene2 = selectedInds[i+1].getGene(j);
                    
                    selectedInds[i].setGene(j, gene2);
                    selectedInds[i+1].setGene(j, gene1);
                }
            }
        }
        
        //Matution
        //Flip Mutation
        for(int i = 0; i < popSize; i++){
            for(int j = 0; j < genSize; j++){
                if(RandomPool.r().nextDouble() < mutationProb){
                    int val = selectedInds[i].getGene(j);
                    selectedInds[i].setGene(j, 1-val);
                }
            }
        }
        
        //Gerational substitution
        for(int i = 0; i < popSize; i++){
            population[i] = selectedInds[i].copy();
        }
        
        iteration++;
        updateFitness();
    }
    
    private void updateFitness(){
        minFit  = Double.POSITIVE_INFINITY;
        meanFit = 0.0;
        maxFit  = Double.NEGATIVE_INFINITY;
        
        for(int i = 0; i < popSize; i++){
            
            double loc[] = new double[pb.getD()];
            String binSol = population[i].getStrCromossome();
            int cLeng = genSize/pb.getD();
            
            for(int j = 0; j < pb.getD(); j++){
                //Binary Solution
                String CoordSol = binSol.substring(j*cLeng, (j+1)*cLeng);
                //Converted to interval [0,1]
                long val = Integer.parseInt(CoordSol,2);
                double normSol = ((double) val)/(Math.pow(2,cLeng));
                //Converted to interval [LB, UB] of the problem
                double coord =  normSol * (pb.getUB()-pb.getLB()) - Math.abs(pb.getLB());
                
                loc[j] = coord;
            }
            
            double v = calculateFunction(loc);
            double f = CalculateFitness(v);
            population[i].setFitness(f);
            
            meanFit += f/popSize;
            
            if(f < minFit) minFit = f;
            if(f > maxFit) maxFit = f;
            
            if(v < globalMin){
	        globalMin = v;
	        globalMinPos = loc;
	    }
        }
    }
    
    public boolean hasFinished(){
        return iteration >= maxInter;
    }
    
    public double[] getGlobalMinPos(){
        return globalMinPos;
    }
    
    public double getGlobalMin(){
        return globalMin;
    }
    
    /*Fitness function*/
    private double CalculateFitness(double fun){
        //Fitness to be maximized
        return -fun;
    }

    private double calculateFunction(double sol[]){
        return pb.toOptimize(sol);
    }
    
    public void setProblem(int idx){
        pb.setProblem(idx);
    }
}
