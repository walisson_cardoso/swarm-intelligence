

package Main;

import ABC.BeeColony;
import GA.GA;
import PSO.PSO;

public class SwarmIntelligence {

    static BeeColony bee;
    static PSO pso;
    //static GA ga = new GA(100, 80, 1000);
    

    public static void main(String[] args) {

        int repetitions = 10;
        Problem pbSample = new Problem();
        
        System.out.println("Optimized parameters by ABC: ");
        
        for(int i = 0; i < 7; i++){
            
            double gp[];
            double meanGp[] = new double[4];
            double gv = 0.0;
            double meanGv = 0.0;
            
            for(int rept = 0; rept < repetitions; rept++){
                bee = new BeeColony(40, 10, 1000, i);
                //pso = new PSO(40, 1000, i);
                
                bee.execute();

                gp = bee.getGlobalMinPos();
                meanGp = new double[gp.length];
                
                for(int x = 0; x < gp.length; x++)
                    meanGp[x] += gp[x]/repetitions;
                
                gv = bee.getGlobalMin();
                meanGv += gv/repetitions;
                
                //System.out.print(gv + " ");
            }
            
            //System.out.println("Problem " + i);
            /*for(int j = 0; j < meanGp.length; j++){
                //System.out.printf("GlobalParam[%d]: %5.14f\n",j,gp[j]);
                System.out.print(meanGp[j] + " ");
            }*/
            
            System.out.println(meanGv);
            
            //System.out.print(Math.abs(pbSample.getOptimum(i)-meanGv));
            //System.out.print(" ");
        }
    }
}

