/*
 * This class organizes a set o problems. Can be used on the algorithms implemented
 */
package Main;

public class Problem {
    
    /* Problem specific variables*/
    private int     D =  4;   /*The number of parameters of the problem to be optimized*/
    private double ub =  5.12; /*lower bound of the parameters. */
    private double lb = -5.12; /*upper bound of the parameters. lb and ub can be defined as arrays for the problems of which parameters have different bounds*/
    
    private int problemIdx = 0; /*Rastringin's function*/
    
    public double toOptimize(double sol[]){
        
        switch(problemIdx){
            case 0:
                return Rastrigin(sol);
            case 1:
                return Rosenbrock(sol);
            case 2:
                return Griewank(sol);
            case 3:
                return Ackley(sol);
            case 4:
                return Michaelewicz(sol);
            case 5:
                return Schaffer2(sol);
            case 6:
                return Eggholder(sol);
            default:
                return 0; //Algum problema ocorreu
        }
    }
    
    public void setProblem(int idx){
        problemIdx = idx;
        D = 4; //Default for multidimentional problems
        
        switch(idx){
            case 0: //Rastringin
                ub = +5.12;
                lb = -5.12;
                break;
            case 1: //Rosenbrock
                ub = +10.0;
                lb = -5.0;
                break;
            case 2: //Griewank
                ub = +600.0;
                lb = -600.0;
                break;
            case 3: //Ackley
                ub = +32.769;
                lb = -32.768;
                break;
            case 4: //Michaelewicz
                D = 2;
                ub = Math.PI;
                lb = 0.0;
                break;
            case 5: //Schaffer2
                D = 2;
                ub = +100.0;
                lb = -100.0;
                break;
            case 6: //Eggholder
                D = 2;
                ub = +512;
                lb = -512;
                break;
            default:
                System.err.println("Problems' indexes go from 0 to 6. You used " + idx);
        }
    }
    
    public double getOptimum(int funIdx){
        
        switch(funIdx){
            case 0:
                return 0.0;
            case 1:
                return 0.0;
            case 2:
                return 0.0;
            case 3:
                return 0.0;
            case 4:
                return -1.8013;
            case 5:
                return 0.0;
            case 6:
                return -959.6407;
            default:
                System.err.println("Problems' indexes go from 0 to 6. You used " + funIdx);
                return -100;
        }
        
    }
    
    public int getD(){
        return this.D;
    }
    
    public double getUB(){
        return this.ub;
    }
    
    public double getLB(){
        return this.lb;
    }
    
    private double Rastrigin(double sol[]){
        /* d dimentions
           Min [0,0,...,0]*/
        double top = 0;
        for(int j = 0; j < D; j++){
            top += (Math.pow(sol[j],(double)2) - 10*Math.cos(2*Math.PI*sol[j])+10);
        }
        return top;
    }
    
    private double Rosenbrock(double sol[]){
        /* d dimentions
           Min [0,0,...,0]*/
        int d = sol.length;
        double sum = 0;
        
        for(int i = 0; i < d-1; i++){
            double xi = sol[i];
            double xnext = sol[i+1];
            double nx = 100*Math.pow(xnext-Math.pow(xi,2),2) + Math.pow(xi-1,2);
            sum += nx;
        }

        return sum;
    }
    
    private double Griewank(double sol[]){
        /* d dimentions
           Min [0,0,...,0]*/
        int d = sol.length;
        double sum = 0;
        double prod = 1;
        
        for(int i = 0; i < d; i++){
            double xi = sol[i];
            sum += Math.pow(xi,2)/4000;
            prod *= Math.cos(xi/Math.sqrt(i+1));
        }

        return sum - prod + 1;
    }
    
    private double Ackley(double sol[]){
        /* d dimentions
           Min [0,0,...,0]*/
        int d = sol.length;
        double a = 20;
        double b = 0.2;
        double c = 2*Math.PI;
        
        double sum1 = 0;
        double sum2 = 0;
        
        for(int i = 0; i < d; i++){
            double xi = sol[i];
            sum1 += Math.pow(xi, 2);
            sum2 += Math.cos(c*xi);
        }
        
        double term1 = -a*Math.exp(-b*Math.sqrt(sum1/d));
        double term2 = -Math.exp(sum2/d);

        return term1 + term2 + a + Math.exp(1);
    }
    
    private double Michaelewicz(double sol[]){
        /* d dimentions
           Min d=2 [2.20, 1.57]*/
        int m = 10;
        int d = sol.length;
        double sum = 0;
        
        for(int i = 0; i < d; i++){
            double xi = sol[i];
            double nx = Math.sin(xi) * Math.pow(Math.sin((i+1)*Math.pow(xi,2)/Math.PI),2*m);
            sum += nx;
        }

        return -sum;
    }
    
    private double Schaffer2(double sol[]){
        /* 2 dimentions
           Min d=2 [0, 0]*/
        double x1 = sol[0];
        double x2 = sol[1];

        double fact1 = Math.pow(Math.sin(Math.pow(x1,2)-Math.pow(x2,2)),2) - 0.5;
        double fact2 = Math.pow(1+0.001*(Math.pow(x1,2)+Math.pow(x2,2)),2);

        return 0.5 + fact1/fact2;
    }
    
    private double Eggholder(double sol[]){
        /* 2 dimentions
           Min d=2 [512, 404.2319]*/
        double x1 = sol[0];
        double x2 = sol[1];

        double term1 = -(x2+47) * Math.sin(Math.sqrt(Math.abs(x2+x1/2+47)));
        double term2 = -x1 * Math.sin(Math.sqrt(Math.abs(x1-(x2+47))));

        return term1 + term2;
    }
}
