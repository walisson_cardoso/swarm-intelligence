
package Random;


public class RandomPool {
    
    private static MersenneTwisterFast instance = null;
    
    protected RandomPool(){
        
    }
    
    public static MersenneTwisterFast r(){
        if(instance == null)
            instance = new MersenneTwisterFast(System.currentTimeMillis());
        return instance;
    }
}
